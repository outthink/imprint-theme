<div id="posts">
		<div class="container">
			<div class="row">
				<?php if (is_single()): ?>
					<div class="col-sm-9 centered">
						<?php echo imprint_breadcrumbs(); ?>
				<?php else : ?>
					<div class="col-sm-8">
				<?php endif; ?>

					<?php $c = 1; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php // found in content.php -- essentially the post data
						get_template_part('content'); ?>
					<?php $c++; endwhile; ?>
					<div class="pages">
						<?php
							global $wp_query;
							$big = 999999999; // need an unlikely integer
							$pages = paginate_links( array(
								'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
								'format' => 'page/%#%',
								'mid_size' => 2,
								'end_size' => 2,
								'prev_text'          => __('<i class="fa fa-chevron-left"></i>'),
								'next_text'          => __('<i class="fa fa-chevron-right"></i>'),
								'current' => max( 1, get_query_var('paged') ),
								'total' => $wp_query->max_num_pages,
								'type' => 'array',
							));
							if( is_array( $pages ) ) {
						        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
						        echo '<ul class="pagination pagination-centered">';
						        foreach ( $pages as $page ) {
						                echo "<li>$page</li>";
						        }
						       echo '</ul>';
							}
						?>
					</div>
					<?php endif; ?>
				</div>
				<?php if (!is_single()): ?>
				<div class="col-sm-3 col-sm-offset-1 sidebar">
					<?php dynamic_sidebar('blog'); ?>
				</div><!-- /sidebar -->
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php get_template_part('footer-widgets'); ?>
