jQuery(function($){
	// this adds "placeholders", or if the browser doesn't support it, adds the label to the text value
	jQuery('.nl-signup-wide label, .front-page-cols label, .mc-field-group label').each(function() {
		var t = jQuery(this);
		if (t.parents('div').hasClass('input-group')) {
		} else {
			var valtext = t.text();
			t.hide();
			t.next('input').attr('placeholder', valtext);

		}
	});
	// this function adds "table" to a TABLE WITHout the table class
	jQuery('table').each( function() {
		var t = $(this);
		if (!t.hasClass('table')) {
			t.addClass('table');
		}
	});
	// this function clears text when the text field is focused
	jQuery('input[type=text], #searchform input, textarea, input[type=email]').each( function() {
		var t = jQuery(this);
		var thisval = t.val();
		t.blur( function() {
			if (t.val() === '') {t.val(thisval);}
		}); // end blur function
		t.focus( function() {
			if (t.val() === thisval) { t.val('');}
		});// end focus function
	}); //END each function

	// this makes a form that is in a class called "newsletter-wrap" inlined
	jQuery('.nl-signup-wide form').addClass('form-inline');

	// add placeholdert text for the search field...esp when hiding the label
	if (jQuery('input#s').val() === '') {
		jQuery('input#s').val('search this site...');
	}

	// all external links open in  new window
	 jQuery("a").filter(function() {
		 return this.hostname && this.hostname !== location.hostname;
	 }).attr('target', '_blank');

	 // function to open/close search dialogue
	 jQuery(function () {
	     jQuery('a[href="#search"]').on('click', function(event) {
	         event.preventDefault();
	         jQuery('#search').addClass('open');
	         jQuery('#search > form > input[type="search"]').focus();
	     });

	     jQuery('#search, #search button.close').on('click keyup', function(event) {
	         if (event.target === this || event.target.className === 'close' || event.keyCode === 27) {
	             jQuery(this).removeClass('open');
	         }
	     });

	 });
}); // END document ready function
function carouselNormalization(carouselID) {
  var items   = jQuery(carouselID + ' .item'),              // grab all the slides
      heights = [],                                   // array to store heights
      tallest;                                        // tallest slide

  if (items.length) {
    function normalizeHeights() {
      items.each(function() {
        heights.push(jQuery(this).height());               // add each slide's height
      });                                             // to the array

      tallest = Math.max.apply(null, heights);        // find the largest height

      items.each(function() {
        jQuery(this).css('min-height', tallest + 'px');    // set each slide's minimum
      });                                             // height to the largest
    };

    normalizeHeights();

    jQuery(window).on('resize orientationchange', function() {
      tallest = 0, heights.length = 0;               // reset the variables

      items.each(function() {
        jQuery(this).css('min-height', '0');              // reset each slide's height
      });

      normalizeHeights();                            // run it again
    });
  }
}
