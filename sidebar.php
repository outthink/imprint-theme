<?php if (is_page()):
		$sidebarcontent = imprint_option( 'sidebar_content', 'imprint_meta' );
		$hidesidebar = imprint_option( 'hide_sidebar', 'imprint_meta' );
		if ($sidebarcontent) { ?>
		<div class="widget sidebar-content">
			<?php
				echo wpautop( do_shortcode($sidebarcontent) );
			?>
		</div>
	<?php }
	// check to see if user is choosing to hide the sidebar.
	if (!$hidesidebar): ?>
		<?php dynamic_sidebar('page'); ?>
	<?php endif; ?>
<?php else : ?>
	<?php dynamic_sidebar('blog'); ?>
<?php endif; ?>
