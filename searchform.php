<form class="form-search" method="get" action="<?php bloginfo('url'); ?>" id="searchform">
	<div class="input-group">
		<input type="text" placeholder="search the site" name="s" type="text" class="form-control">
		<span class="input-group-btn">
			<button class="btn btn-primary" type="button"><span class="fa fa-search"></button>
		</span>
	</div><!-- /input-group -->
</form>
