<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
		<?php wp_head(); ?>
	</head>

</head>

<body id="<?php echo page_handle(); ?>" <?php body_class(); ?>>
	<section class="body-wrap">
    <!-- Static navbar -->
    <?php do_action('imprint_top_nav'); ?>
	<?php do_action('imprint_after_header'); ?>
	<?php do_action('imprint_do_masthead'); ?>
