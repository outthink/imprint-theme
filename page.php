<?php get_header(); ?>
	<div class="container page content">
		<div class="row">
			<?php
			// most of the stuff for the pages needs to be determined by some options, so I'm pulling in a template part that handles that
			get_template_part('content-page'); ?>
		</div>
	</div><!--end container -->
<?php get_footer(); ?>
