<?php get_header(); ?>
<section>
	<header class="jumbotron jt-page">
		<?php if ($masthead_img = imprint_option('jumbotron_image', 'imprint_meta')):
			if ( $imgsrc = wp_get_attachment_image_src( $masthead_img, 'full') ) { ?>
			<style type="text/css" media="screen">
				.jumbotron {
					position: relative;
				}
				body .jt-page {
					padding-top: 200px;
					padding-bottom: 200px;
				}
				body .jt-page h1 {
					font-size: 36px;
				}
				.jumbotron::after {
					content: "";
					background: url('<?php echo $imgsrc[0]; ?>') top center;
					background-size: cover;
					opacity: 0.2;
					top: 0;
					left: 0;
					bottom: 0;
					right: 0;
					position: absolute;
					z-index: 1;
				}
				<?php if ( $color = imprint_option('headline_color', 'imprint_meta') ) {
					echo '.jt-page h1 {color: '.$color.'}';
				} ?>
				.jumbotron > * {
					position: relative;
					z-index: 2;
				}
			</style>
			<?php } ?>
		<?php endif; ?>
		<div class="bgimg">
			<div class="container">
				<div class="col-md-8 centered">
					<h1 class="pagetitle">
						<a href="<?php bloginfo('url'); ?>/books">Books</a>
					</h1>
					<?php if ($subtext = get_post_meta($post->ID, 'imprint_page_subtext', true)): ?>
						<h2><?php echo $subtext;?></h2>
					<?php endif ?>
				</div><!--end col-md-8 centered -->
			</div><!--end container -->
		</div><!--end bgimg -->
	</header><!--end jumbotron -->
</section>
	<div class="container books page content">
		<?php if (have_posts()) : ?>
		<div class="row">
			<?php
			while (have_posts()) : the_post();
			$bookdata = ot_bookdata($post->ID);
			$amazon = $bookdata['amazon'];
			$bn = $bookdata['bn'];
			$ceoread = $bookdata['ceoread'];
			$indie = $bookdata['indie'];
			$ibooks = $bookdata['ibooks'];
			$itunes = $bookdata['itunes'];
			$audible = $bookdata['audible'];
			$bam = $bookdata['bam'];
			$subtitle = $bookdata['subtitle'];
			?>
			<div class="col-sm-4 sidebar">
				<div class="widget book">
					<?php if ($amazon): ?>
						<a href="<?php echo $amazon; ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
					<?php elseif($bn) :?>
						<a href="<?php echo $bn; ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
					<?php elseif($ceoread) :?>
						<a href="<?php echo $ceoread; ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
					<?php else :?>
						<?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?>
					<?php endif; ?>
					<?php if (is_single()): ?>
						<?php if (function_exists('ot_booklinks')):
							echo ot_booklinks($bookdata);
						endif; ?>
					<?php endif; ?>

				</div><!--END widget-->
				<?php if (is_single()):
					dynamic_sidebar('book');
				endif; ?>
			</div><!--end span5 -->
			<div class="col-sm-8 entry">
				<div class="post-content">
						<?php if (is_single()): ?>
							<div class="headline">
				                <h1 class="pagetitle">
									<?php the_title(); ?><?php if ($subtitle): echo ': <span class="book_subtitle">'.$subtitle.'</span>'; endif; ?>
								</h1>
				            </div>
						<?php else : ?>
							<h2 class="book_title">
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?><?php if ($subtitle): echo ': <span class="book_subtitle">'.$subtitle.'</span>'; endif; ?>
								</a>
							</h2>
						<?php endif; ?>
					<?php if (!is_single()): ?>
						<?php the_excerpt(); ?>
					<?php if (function_exists('ot_booklinks')):
						if ($amazon) {
							echo '<p class="order-from">Order From:</p>';
						}
						echo ot_booklinks($bookdata);
					endif; ?>
					<?php else : ?>
						<?php the_content(); ?>
					<?php endif; ?>
				</div><!--END post-content-->
			</div><!--END entry-->
			<div class="clr"></div>
		<?php endwhile; ?>
		</div><!--end row -->
		<?php endif; ?>
	</div><!--end container -->


<?php get_footer(); ?>
