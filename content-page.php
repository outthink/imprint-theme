<?php
// lets setup some stuff about the page layout
$global_sidebar = imprint_option('sidebar_layout');
//$enable_global_sidebar_layout = imprint_option( 'enable_global_sidebar_layout' );

// by default, there's a sidebar...so...there 
$sidebar = true;
// by default, the heading is shown on the page.
$heading = true;

// setting up main column classes.
$col_class = 'col-md-8 col-sm-8 col-lg-7';

// sidebar_layout, determined here.
$sb_meta_pos = imprint_option( 'sidebar_layout', 'imprint_meta' );

// get the page template
$template = basename( get_page_template() );

switch ($template) {
    case 'template-jumbotron.php':
        $heading = false;

        break;
    default:
        break;
}
// sidebar class is setup here.
$sidebar_class = 'col-md-4 col-sm-4 col-lg-4';

if( $sb_meta_pos == 'global' ){
    $sidebar_position = $global_sidebar;
}else{
     $sidebar_position = $sb_meta_pos;
}
// if ( !empty( $sb_meta_pos ) ) {
//     $sidebar_position = $sb_meta_pos;
// } else {
//     $sidebar_position = $global_sidebar;
// }

if ($sidebar_position == 'sidebar_right') {
    $sidebar_class .= ' col-lg-offset-1';
} elseif ( $sidebar_position == 'no_sidebar_center' ) {
    $sidebar = false;
    $col_class = 'col-md-8 centered';
}elseif ( $sidebar_position == 'no_sidebar_full' ) {
    $sidebar = false;
    $col_class = 'col-md-12';
} 
else {
    // if the sidebar position IS right, add these classes to give it some room
    $col_class .= ' col-lg-offset-1 pull-right';
}
?>
<div class="<?php echo $col_class; ?> entry">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php if (true == $heading) { ?>
            <div class="headline">
                <h1 class="pagetitle"><?php the_title(); ?></h1>
            </div>
        <?php } // end check for heading ?>
        <div class="post post-<?php echo get_the_ID(); ?>">
            <?php the_content(); ?>
        </div><!--END post-->
    <?php endwhile; ?>
    <?php endif; ?>
</div><!--end span8 -->

<?php
if (true == $sidebar ): ?>
<div class="sidebar <?php echo $sidebar_class; ?>">
    <?php get_sidebar(); ?>
</div>
<?php endif; ?>
