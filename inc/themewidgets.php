<?php
// initializes the widget on WordPress Load
add_action('widgets_init', 'nlsignup_init_widget');

	// ********
	// This is a widget for the newsletter signup
	function nlsignup_init_widget() {
		register_widget( 'NL_Signup_Widget' );
	}


	// new class to extend WP_Widget function
	class NL_Signup_Widget extends WP_Widget {
		/** Widget setup.  */
		function __construct() {
			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'nlsignup_widget',
				'customize_selective_refresh' => true,
				'description' => __('Widget for Newsletter Signup', 'nlsignup_widget') );

			/* Widget control settings. */
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'nl-signup-widget' );

			/* Create the widget. */
			$this->WP_Widget( 'nl-signup-widget', __('IMPRINT: Newsletter Signup Widget', 'Options'), $widget_ops, $control_ops );
		}
		/**
		* How to display the widget on the screen. */
		function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters('widget_title', $instance['title'] );
			$introtext = $instance['introtext'];
			/* Before widget (defined by themes). */
			echo $before_widget;
			if ( $title )
				echo $before_title . $title . $after_title;
			/* Display name from widget settings if one was input. */

			// Settings from the widget
		?>
			<div class="newsletter-widget">
				<?php if ($introtext): ?>
					<?php echo $introtext; ?>
				<?php else : ?>
					<?php echo imprint_newsletter('introtext'); ?>
				<?php endif ?>
				<?php echo imprint_nlsignup(); ?>
			</div>
			<?php
			/* After widget (defined by themes). */
			echo $after_widget;
		}

		/**
		 * Update the widget settings.
		 */
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			/* Strip tags for title and name to remove HTML (important for text inputs). */
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['introtext'] = $new_instance['introtext'];

			return $instance;
		}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
*/
		function form( $instance ) {
			$defaults = array( 'title' => __('Newsletter Signup', 'nlsignup_widget'));
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
			<!-- Widget Title: Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'nlsignup_widget'); ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'introtext' ); ?>"><?php _e('Newsletter Intro Text:', 'nlsignup_widget'); ?> <br><small>Optional, if not provided, the intro text in the imprint settings will be used</small></label>
				<textarea id="<?php echo $this->get_field_id( 'introtext' ); ?>" name="<?php echo $this->get_field_name( 'introtext' ); ?>" class="widefat"><?php echo $instance['introtext']; ?></textarea>
			</p>
			<p>This widget pulls the content from the <a href="<?php admin_url('admin.php?page=imprint'); ?>">Newsletter Embed</a> code.</p>
		<?php
		}
	}

// This is a widget for Say Hello

// initializes the widget on WordPress Load
add_action('widgets_init', 'ot_social_init_widget');

	// Should be called above from "add_action"
	function ot_social_init_widget() {
		register_widget( 'OT_Social_Widget' );
	}

	// new class to extend WP_Widget function
	class OT_Social_Widget extends WP_Widget {
		/** Widget setup.  */
		function __construct() {
			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'ot_social_widget',
				'customize_selective_refresh' => true,
				'description' => __('Say Hello', 'ot_social_widget') );

			/* Widget control settings. */
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ot_social-widget' );

			/* Create the widget. */
			$this->WP_Widget( 'ot_social-widget', __('IMPRINT: Say Hello Widget', 'Options'), $widget_ops, $control_ops );
		}
		/**
		* How to display the widget on the screen. */
		function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters('widget_title', $instance['title'] );

			/* Before widget (defined by themes). */
			echo $before_widget;
			/* Display name from widget settings if one was input. */

			if ( $title )
				echo $before_title . $title . $after_title;

			// Settings from the widget
			imprint_social();

			/* After widget (defined by themes). */
			echo $after_widget;
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			/* Strip tags for title and name to remove HTML (important for text inputs). */
			$instance['title'] = strip_tags( $new_instance['title'] );

			return $instance;
		}
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
*/
		function form( $instance ) {
			$defaults = array( 'title' => __('Say Hello', 'ot_social_widget'));
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ot_social_widget'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
			<p>This widget pulls your social media links using your profile links you setup in the customizer.</p>
		<?php
		}
	} // END OT_Social_Widget


	// This is a widget for About the Author
	// initializes the widget on WordPress Load
	add_action('widgets_init', 'ot_about_init_widget');

	// Should be called above from "add_action"
	function ot_about_init_widget() {
		register_widget( 'OT_About_Widget' );
	}

	// new class to extend WP_Widget function
	class OT_About_Widget extends WP_Widget {
		/** Widget setup.  */
		function __construct() {
			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'ot_about_widget',
				'customize_selective_refresh' => true,
				'description' => __('About the Author', 'ot_about_widget') );
			/* Widget control settings. */
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ot_about-widget' );
			/* Create the widget. */
			$this->WP_Widget( 'ot_about-widget', __('IMPRINT: About the Author Widget', 'Options'), $widget_ops, $control_ops );
		}
	/* How to display the widget on the screen. */
	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title'] );
		global $post;
		/* Before widget (defined by themes). */
			if ($post->ID != imprint_setting('about_page')) {
				echo $before_widget;

				/* Display name from widget settings if one was input. */
				if ( $title )
					echo $before_title . $title . $after_title;

				// Settings from the widget
				imprint_about_widget_text();
				/* After widget (defined by themes). */
				echo $after_widget;
			} // end check for about page

		} // end widget (function)
		// updates the widget
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			/* Strip tags for title and name to remove HTML (important for text inputs). */
			$instance['title'] = strip_tags( $new_instance['title'] );

			return $instance;
		}
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	*/
	function form( $instance ) {
		$defaults = array( 'title' => __('About the Author', 'ot_about_widget'));
		$about_page = imprint_setting('about_page');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	<!-- Widget Title: Text Input -->

			<!-- Widget Title: Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ot_about_widget'); ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
			</p>
			<p>This widget displays a short blurb about the author, along with the featured image from the <a href="<?php echo get_bloginfo("wp_url").'/wp-admin/post.php?post='.$about_page.'&action=edit'; ?>">About page</a>.</p>
		<?php
		}
	} // END OT_About_Widget


// This is a widget for Tabber Widget

// initializes the widget on WordPress Load
add_action('widgets_init', 'ot_tabber_widget_init_widget');

// Should be called above from "add_action"
function ot_tabber_widget_init_widget() {
	register_widget( 'ot_tabber_widget' );
}

// new class to extend WP_Widget function
class ot_tabber_widget extends WP_Widget {
	/** Widget setup.  */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array(
			'classname' => 'ot_tabber_widget_widget',
			'customize_selective_refresh' => true,
			'description' => __('This widget provides a tabbed area based on whatever widgets you added to the Tabber Widget Area', 'ot_tabber_widget_widget') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ot_tabber_widget_widget' );

		/* Create the widget. */
		$this->WP_Widget( 'ot_tabber_widget_widget', __('Tabber Widget', 'Options'), $widget_ops, $control_ops );
	}
	/**
	* How to display the widget on the screen. */
	function widget( $args, $instance ) {
		extract( $args );

		/* Before widget (defined by themes). */
		echo $before_widget;

		// Settings from the widget
		?>
		<ul class="nav nav-tabs" id="myTab" style="display:none;">
		</ul>
		<div class="tab-content">
			<?php dynamic_sidebar('ot_tabber'); ?>
		</div>
		<script>
		jQuery(document).ready(function() {
			jQuery('.tab-content .tab-pane').each(function() {
				var t = jQuery(this);
				var tID = t.attr('id');
				var theading = t.find('h3.widgettitle');
				// removing the class widget because it's not really a standalone widget anymore...
				t.removeClass('widget');
				// adding the class "active" so that the first selected widget shows up.
				theading.remove();
				jQuery('#myTab').append('<li><a href="#'+tID+'">'+theading.text()+'</a></li>');
				jQuery('.tab-content div').first().addClass('active');
				jQuery('#myTab li').first().addClass('active')
				jQuery('#myTab').show();
				t.find('li:even').addClass('alt');
			});
			jQuery('#myTab a').click(function (e) {
			  e.preventDefault();
			  jQuery(this).tab('show');
			});

		});
		</script>
<?php	/* After widget (defined by themes). */
		echo $after_widget;
	}

/**
 * Displays the widget settings controls on the widget panel.
 * Make use of the get_field_id() and get_field_name() function
 * when creating your form elements. This handles the confusing stuff.
*/
	function form( $instance ) { ?>
		<!-- Widget Title: Text Input -->
		<p>This widget provides a tabbed widget based on whatever widgets you add to the Tabber Widget Area.</p>
	<?php
	}
} // END ot_tabber_widget
