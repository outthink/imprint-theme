<?php
// [newsletter text='Text before the newsletter']
function ot_newsletter_function($atts) {
	$nlsignup_introtext = imprint_newsletter('introtext');
	extract(shortcode_atts(array(
		"text" => ""
    ), $atts));
		$retcont = '';
		$retcont .= '<div class="nlsignup_content form-inline">';
		if (!empty($text)) {
			$retcont .= wpautop($text);
		} elseif (!empty($nlsignup_introtext)) {
			$retcont .= wpautop($nlsignup_introtext);
		}
		$retcont .= imprint_nlsignup() . '</div>';
		return $retcont;

}
add_shortcode("newsletter", "ot_newsletter_function");
