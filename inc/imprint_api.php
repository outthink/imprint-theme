<?php
// Imprint API
// helper functions to get options
//function imprint_option($setting = 'settings', $key = '') {
//	$value = (array)get_option('imprint-'.$setting);
//	if ($key != '') {
//		return $value[$key];
//	} else {
//		return $value;
//	}
//}

function imprint_setting($handle) {
	return imprint_option($handle);
}
function imprint_newsletter($handle) {
	return imprint_option($handle);
}


function imprint_option($setting, $key = 'imprint') {
	$titan = TitanFramework::getInstance( $key );
    return $titan->getOption( $setting );
}

// replaced the social settings here with the actual get_theme_mod function
function imprint_social_settings( $key = '' ) {
	// get the imprint social
	$settings = get_theme_mod('imprint_social');
	if (strlen($key) > 0) {
		return $settings[$key];
	} else {
		return $settings;
	}
}

function imprint_home_option( $myoption) {
    $titan = TitanFramework::getInstance( 'imprint-theme-home' );
    return $titan->getOption( $myoption );
}
