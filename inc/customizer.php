<?php
class ImprintHomepageOptions {

    function __construct() {

        add_action( 'tf_create_options', array( $this, 'createImprintOptions' ) );
    }

    function createImprintOptions() {

        // Initialize primary imprint options (theme mods only here);
        $imprintsettings = TitanFramework::getInstance( 'imprint' );

        //Logo
        $logoPanel = $imprintsettings->createThemeCustomizerSection( array(
            'id' => 'title_tagline',
            'position' => 12,
        ) );

        $logoPanel->createOption( array(
            'name' => 'Upload your Logo',
            'type' => 'upload',
            'id'   => 'site_logo',  
        ) );

        $logoPanel->createOption( array(
            'name' => 'Adjust Logo Width',
            'id' => 'site_logo_width',
            'type' => 'number',
            'desc' => 'You can change the width of logo. Max width is <b>300 pixels</b>',
            'default' => '100',
            'max' => '300',
            'unit' => 'px',
        ) );

        //Imprint Settings
        $imprintpanel = $imprintsettings->createThemeCustomizerSection( array(
            'name' => 'Imprint Settings',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Author Name',
            'id' => 'author_name',
            'type' => 'text',
            'default' => 'Add your name.',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'About Page',
            'id' => 'about_page',
            'type' => 'select-pages',
            'desc' => 'Please select your about page'
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Choose your color scheme:',
            'id' => 'color_scheme',
            'type' => 'select',
            'desc' => 'This option will allow you to change the color pallet',
            'options' => array(
                '1' => 'Blue',
                '2' => 'Yellow',
                '3' => 'Red',
                '4' => 'Pink',
            ),
            'default' => '1',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Enable Search',
            'id' => 'search_enabled',
            'type' => 'enable',
            'default' => true,
            'desc' => 'Enable or disable search button',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Enable Social Links',
            'id' => 'social_enabled',
            'type' => 'enable',
            'default' => true,
            'desc' => 'Enable or disable social links',
        ) );


        //Colors
        $imprintpanel->createOption( array(
            'name' => 'Navbar Colors',
            'type' => 'heading'      
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Enable Custom Navbar Colors',
            'id' => 'navbar_colors_enabled',
            'type' => 'checkbox',
            'default' => false,
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Navbar Background Color',
            'id' => 'navbar_background_color',
            'type' => 'color',
            'default' => '#f8f8f8',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Active Link Background Color',
            'id' => 'navbar_active_color',
            'type' => 'color',
            'default' => '#e7e7e7;',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Link Color',
            'id' => 'navbar_link_color',
            'type' => 'color',
            'default' => '#777777',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Link Hover Color',
            'id' => 'navbar_link_hover_color',
            'type' => 'color',
            'default' => '#333333',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Custom Colors',
            'type' => 'heading'      
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Use custom colors instead of color scheme',
            'id' => 'enable_color_overlay',
            'type' => 'checkbox',
            'desc' => '',
            'default' => false,
        ) );
        
        $imprintpanel->createOption( array(
            'name' => 'Primary Color',
            'id' => 'primary_color',
            'type' => 'color',
            'desc' => 'Will be used for links, button and sections',
            'default' => '#337ab7',
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Secondary Color',
            'id' => 'secondary_color',
            'type' => 'color',
            'desc' => 'Will be used on hover for links, button and sections',
            'default' => '#286090',
        ) );

        // $imprintpanel->createOption( array(
        //     'name' => 'Navbar Background Color',
        //     'id' => 'navbar_bck_color',
        //     'type' => 'color',
        //     'default' => '#337ab7',
        // ) );

        $imprintpanel->createOption( array(
            'name' => 'Footer Background Color',
            'id' => 'footer_bck_color',
            'type' => 'color',
            'default' => '#555555',
        ) );


       

        //Site Layout
        $imprintpanel->createOption( array(
            'name' => 'Site Layout',
            'type' => 'heading'      
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Select website layout',
            'id' => 'website_layout',
            'type' => 'select',
            'options' => array(
                'box' => 'Box Layout',
                'full' => 'Full Width Layout',       
            ),
            'default' => 'box',
        ) );

        //Page Layout
        $imprintpanel->createOption( array(
            'name' => 'Page Layout',
            'type' => 'heading'      
        ) );

        // $imprintpanel->createOption( array(
        //     'name' => 'Use global page layout instead of respective page layout',
        //     'id' => 'enable_global_sidebar_layout',
        //     'type' => 'checkbox',
        //     'desc' => '',
        //     'default' => false,
        // ) );
        $imprintpanel->createOption( array(
            'name' => 'Global Page Layout',
            'desc' => 'Select a default page layout for <b>all pages</b>',
            'id' => 'sidebar_layout',
            'type' => 'radio-image',
            'options' => array(
                'sidebar_left' => get_template_directory_uri() . '/inc/img/sidebar_left.png',
                'sidebar_right' => get_template_directory_uri() . '/inc/img/sidebar_right.png',
                'no_sidebar_center' => get_template_directory_uri() . '/inc/img/no_sidebar_center.png',
                'no_sidebar_full' => get_template_directory_uri() . '/inc/img/no_sidebar.png',
            ),
            'default' => 'sidebar_left',
        ) );

        //Custom Scripts
        $imprintpanel->createOption( array(
            'name' => 'Custom Scripts',
            'type' => 'heading'      
        ) );
        $imprintpanel->createOption( array(
            'name' => 'Header Scripts',
            'id' => 'header_scripts',
            'type' => 'textarea',
            'is_code' => true,
            'desc' => 'Any code you enter here will be added to the header.',
        ) );
        $imprintpanel->createOption( array(
            'name' => 'Footer Scripts',
            'id' => 'footer_scripts',
            'type' => 'textarea',
            'is_code' => true,
            'desc' => 'Any code you enter here will be added to the footer.',
        ) );

        //Content Setings
        $imprintpanel->createOption( array(
            'name' => 'Content Setings',
            'type' => 'heading'      
        ) );

        $imprintpanel->createOption( array(
            'name' => 'Full Post Content?',
            'id' => 'full_content',
            'type' => 'checkbox',
            'desc' => 'Check this to show the full post content on the blog. <em>Defaults to excerpt</em>.',
            'default' => false,
        ) );

        $socialpanel = $imprintsettings->createThemeCustomizerSection( array(
            'name' => 'Imprint Social',
            'id' => 'imprint_social'
        ) );
        $socialpanel->createOption( array(
            'name' => 'Twitter',
            'id' => 'social_twitter',
            'type' => 'text',
            'desc' => 'Your Twitter Handle (ex: jhinson).',
        ) );
        $socialpanel->createOption( array(
            'name' => 'Facebook Profile URL',
            'id' => 'social_facebook',
            'type' => 'text',
            'desc' => 'Including http://',
        ) );
        $socialpanel->createOption( array(
            'name' => 'Instagram URL',
            'id' => 'social_instagram',
            'type' => 'text',
            'desc' => 'Including http://',
        ) );
        $socialpanel->createOption( array(
            'name' => 'Google Plus URL',
            'id' => 'social_google-plus',
            'type' => 'text',
            'desc' => 'Including http://',
        ) );
        $socialpanel->createOption( array(
            'name' => 'LinkedIn Profile URL',
            'id' => 'social_linkedin',
            'type' => 'text',
            'desc' => 'Including http://',
        ) );
        $socialpanel->createOption( array(
            'name' => 'Youtube URL',
            'id' => 'social_youtube',
            'type' => 'text',
            'desc' => 'Including http://',
        ) );
        $socialpanel->createOption( array(
            'name' => 'Pinterest URL',
            'id' => 'social_pinterest',
            'type' => 'text',
            'desc' => 'Including http://',
        ) );

        if ( apply_filters('imprint_show_home_customizer_options', true, $homeoptions) ) {
        	// blow up da options, yo
            // Homepage options
            // Initialize Titan & options here
            $homeoptions = TitanFramework::getInstance( 'imprint-theme-home' );
            $homepanel = $homeoptions->createThemeCustomizerSection( array(
                'name' => 'Masthead Details',
                'panel' => 'Homepage Details'
            ) );
            // Masthead Options
            $homepanel->createOption( array(
                'name' => 'Masthead Options',
                'type' => 'heading',
            ) );
            $homepanel->createOption( array(
                'name' => 'Masthead Image',
                'id' => 'masthead_image',
                'type' => 'upload',
                'desc' => 'Upload your image'
            ) );

            $homepanel->createOption( array(
                'name' => 'Masthead Tagline',
                'id' => 'masthead_tagline',
                'type' => 'textarea',
                'desc' => 'Enter your tagline',
                'default' => 'Use this area to create a compelling tagline. Something that says something about why what you do matters.',
                //'transport' => 'postMessage'
            ) );

            $homepanel->createOption( array(
                'name' => 'Tagline Color',
                'id' => 'masthead_tagline_color',
                'type' => 'color',
                'desc' => 'In case you need to change the tagline color to something darker.',
                'default' => '#ffffff'
            ) );

            $homepanel->createOption( array(
                'name' => 'Use Email Opt-In instead of button?',
                'id' => 'email_opt_in',
                'type' => 'checkbox',
                'desc' => 'If you would like to feature your email opt-in instead of a button to your about page, check this box',
                'default' => false,
            ) );

            $homepanel->createOption( array(
                'name' => 'Tagline Link URL',
                'id' => 'tagline_link',
                'type' => 'text',
                'desc' => 'Where does your button link?',
                'default' => get_bloginfo('url')
            ) );

            $homepanel->createOption( array(
                'name' => 'Tagline Link Text',
                'id' => 'tagline_link_text',
                'type' => 'text',
                'desc' => 'What does your button say?',
                'default' => 'Learn more'
            ) );

            // secondary Section
            $secondarypanel = $homeoptions->createThemeCustomizerSection( array(
                'name' => 'Secondary Content',
                'panel' => 'Homepage Details'
            ) );

            $secondarypanel->createOption( array(
                'name' => 'Secondary Content',
                'type' => 'heading',
            ) );

            $secondarypanel->createOption( array(
                'name' => 'Image',
                'id' => 'secondary_image',
                'type' => 'upload',
                'desc' => 'Choose the image for the secondary section',
            ) );

            $secondarypanel->createOption( array(
                'name' => 'Section title',
                'id' => 'secondary_title',
                'type' => 'text',
                'desc' => 'Enter the title for your secondary section.',
                'default' => 'About The Author'
            ) );

            $secondarypanel->createOption( array(
                'name' => 'secondary Text',
                'id' => 'secondary_text',
                'type' => 'textarea',
                'desc' => 'Enter your tagline',
                'default' => 'Use this area to talk about yourself, or an additional service you provide.

    This could be speaking, consulting, or simply an about section. <a href="#">Learn More.</a>',
            ) );

            $bookpanel = $homeoptions->createThemeCustomizerSection( array(
                'name' => 'Book Options',
                'panel' => 'Homepage Details'
            ) );

            $bookpanel->createOption( array(
                'name' => 'Featured Book',
                'id' => 'featured_book',
                'type' => 'select-posts',
                'desc' => 'Select your book from the dropdown. <em>If you do not select a book, no book will be shown on the homepage.</em>',
                'post_type' => 'book',
            ) );

            $bookpanel->createOption( array(
                'name' => 'Use buy links.',
                'id' => 'use_buy_links',
                'type' => 'checkbox',
                'desc' => 'If you would like to use book links here, check this box.',
                'default' => false,
            ) );

            $bookpanel->createOption( array(
                'name' => 'Show All Books Link?',
                'id' => 'show_link_to_all_books',
                'type' => 'checkbox',
                'desc' => 'Check this to show a link to "all" books',
                'default' => false,
            ) );

            // Reviews
            $reviews_panel = $homeoptions->createThemeCustomizerSection( array(
                'name' => 'Review Options',
                'panel' => 'Homepage Details'
            ) );
            $reviews_panel->createOption( array(
                'name' => 'Select Review Category',
                'id' => 'review_source',
                'type' => 'select-categories',
                'desc' => 'Please select the category you would like to use for your reviews',
                'taxonomy' => 'sources',
            ) );
            $reviews_panel->createOption( array(
                'name' => 'Use custom excerpts?',
                'id' => 'review_content',
                'options' => array(
                    'excerpt' => 'Use excerpt',
                    'content' => 'Use full content',
                ),
                'type' => 'radio',
                'desc' => 'Choose whether you want to use a custom excerpt, or the full content of the review.',
                'default' => 'Content',
            ) );
        }
    }
}

new ImprintHomepageOptions();
