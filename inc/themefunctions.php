<?php

function imprint_widgets_count( $sidebar_id )
{
	$sidebars_widgets = wp_get_sidebars_widgets();
	return (int) count( (array) $sidebars_widgets[ $sidebar_id ] );
}

	// Imprint Social Buttons
	if (! function_exists( 'imprint_social' )) {
		function imprint_social($element = 'span') {
			// setup the array that will loop through and spit out both the key, and the value for the social link builder
			$sn = array(
				'twitter' => 'Twitter',
				'facebook' => 'Facebook',
				'instagram' => 'Instagram',
				'google-plus' => 'Google Plus',
				'linkedin' => 'LinkedIn',
				'youtube' => 'Youtube',
				'pinterest' => 'Pinterest'
			);
			// loop through the above array
			foreach ($sn as $key => $value):
				// both check and get the setting...if it doesn't exist, it will just be skipped, if it does exist, it will print the links.
				if ($tsetting = imprint_option('social_'.$key)) {
					if ($key == 'twitter') {
						$link = 'http://twitter.com/'.$tsetting;
					} else {
						$link = $tsetting;
					}
					echo '<'.$element.'><a class="'.$key.'" rel="me" href="'.$link.'" alt="Connect with '.imprint_setting('author_name').' on '.$value.'"><i class="fa fa-'.$key.'"></i> <span class="is_desc_text">' . $value . '</span></a></'.$element.'>';
				}

			endforeach;
		} // END function imprint_social()
	} // end check for function

	if (! function_exists('page_handle')) {
		function page_handle() {
			global $post;
			$queue = '';
			if (is_post_type_archive('book')  or get_post_type() == 'book') {
				$queue = 'books';

			} elseif (is_home() or is_single() or is_category() or is_archive() or is_search()) {
				$queue = 'blog';

			} elseif (is_front_page() or is_page_template('template-home.php')) {
				$queue = 'home';
			} elseif (is_page()) {
				$queue = $post->post_name;
			}
			echo $queue;
		}
	}
	function imprint_remove_parent_classes($class)
	{
	  // check for current page classes, return false if they exist.
		return ($class == 'current_page_item' || $class == 'current_page_parent' || $class == 'current_page_ancestor'  || $class == 'current-menu-item') ? FALSE : TRUE;
	}

	// TODO: Fix this so that the book nav item gets highlighted

	function add_class_to_wp_nav_menu($classes)
	{
		switch (get_post_type())
		{
			case 'book':
				// we're viewing a custom post type, so remove the 'current_page_xxx and current-menu-item' from all menu items.
				$classes = array_filter($classes, "imprint_remove_parent_classes");

				// add the current page class to a specific menu item this requires that the books page have a class "books"
				if (in_array('books', $classes))
		{
	     		   $classes[] = 'current_page_parent current-menu-item';
		}
	     		break;

	      // add more cases if necessary and/or a default
	     }
		return $classes;
	}
	add_filter('nav_menu_css_class', 'add_class_to_wp_nav_menu');

	function imprint_nlsignup() {
		$nlembed = imprint_newsletter('embed_code');
		if (strlen($nlembed) != '') {
			$return = $nlembed;
		} else {
			$return = '<div id="mc_embed_signup">
			<form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
			<div class="mc-field-group">
				<label for="mce-FNAME" style="display: none;">First Name </label>
				<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name ">
			</div>
			<div class="mc-field-group">
				<label for="mce-EMAIL" style="display: none;">Email Address </label>
				<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address ">
			</div>
				<div id="mce-responses" class="clear">
					<div class="response" id="mce-error-response" style="display:none"></div>
					<div class="response" id="mce-success-response" style="display:none"></div>
				</div>
				<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn"></div>
			</form>
			</div>';
		} // end check for nlembed option
		return $return;
	}
	function imprint_nltext() {
		$introtext = imprint_newsletter('introtext');
		if ($introtext) {
			return $introtext;
		} else {
			return 'Subscribe to my newsetter and receive updates directly to your inbox.';
		}
	}

	if (!function_exists('imprint_about_widget_text')) {
		function imprint_about_widget_text() {
			$about_page = imprint_setting('about_page');
			if($about_page != '0') : ?>
				<div class="imprint_about">
					<p>
						<span class="imprint-about-image-wrap"><a href="<?php echo get_permalink($about_page); ?>"><?php echo get_the_post_thumbnail($about_page, 'about-image', array('class' => 'imprint-about-image')); ?></a></span>
						<?php echo get_post_meta($about_page, 'imprint_meta_pageexcerpt', true); ?>
						<span class="imprint-more-link-wrap"><a class="more" href="<?php echo get_permalink($about_page); ?>">Learn More...</a></span>
					</p>
				</div>
			<?php endif;
		}
	}

	// [imprint_embed]
	function imprint_embed_responsive($atts) {
			extract(shortcode_atts(array(
				"video" => ""
	        ), $atts));
			ob_start(); ?>
			<style>
			.embed-container {
				position: relative;
				padding-bottom: 56.25%;
				height: 0;
				overflow: hidden;
				max-width: 100%;
				margin-bottom: 20px;
			}
			.embed-container iframe, .embed-container object, .embed-container embed {
				position: absolute; top: 0; left: 0; width: 100%; height: 100%;
			}
			</style>
			<div class='embed-container'><iframe src='<?php echo $video; ?>' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
			<?php
			$return = ob_get_clean();
			return $return;
	}
	add_shortcode("imprint_embed", "imprint_embed_responsive");
	// end shortcode
	function imprint_breadcrumbs() {
		    global $post;
			ob_start();
				echo '<ul class="breadcrumb">';
			if (!is_home()) {
				echo '<li><a href="';
				echo get_option('home');
				echo '">';
				echo 'Home';
				echo '</a></li> ';
				if (get_post_type() !== 'post') {
					if (get_post_type() == 'resources') {
						if (is_single() or is_tax()) {
							echo "<li>";
							echo '<a href="'.get_bloginfo("url").'/resources">Resources</a>';
							echo '</li>';
						}
						if (is_single()) {
							echo "<li class='active'>";
							the_title();
							echo '</li>';
						}
					}
				}
				elseif (is_category() || is_single()) {
					echo '<li>';
					the_category(' </li><li> ');
					if (is_single()) {
						echo "</li> <li>";
						the_title();
						echo '</li>';
					}

				} elseif (is_page()) {
					$parents = get_post_ancestors( $post );
					asort($parents);
						foreach ($parents as $parent) {
							echo '<li>
								<a href="'.get_permalink($parent).'">'.get_the_title($parent).'</a>
							</li>';
						}
					echo '<li>';
					echo the_title();
					echo '</li>';
				}
			}
			elseif (is_tag()) {single_tag_title();}
			elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
			elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
			elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
			elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
			elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
			elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
			echo '</ul>';
			$return = ob_get_clean();
			return $return;
	}

	/**
	 * Lightens/darkens a given colour (hex format), returning the altered colour in hex format.7
	 * @param str $hex Colour as hexadecimal (with or without hash);
	 * @percent float $percent Decimal ( 0.2 = lighten by 20%(), -0.4 = darken by 40%() )
	 * @return str Lightened/Darkend colour as hexadecimal (with hash);
	 */
	function color_luminance( $hex, $percent ) {

		// validate hex string

		$hex = preg_replace( '/[^0-9a-f]/i', '', $hex );
		$new_hex = '#';

		if ( strlen( $hex ) < 6 ) {
			$hex = $hex[0] + $hex[0] + $hex[1] + $hex[1] + $hex[2] + $hex[2];
		}

		// convert to decimal and change luminosity
		for ($i = 0; $i < 3; $i++) {
			$dec = hexdec( substr( $hex, $i*2, 2 ) );
			$dec = min( max( 0, $dec + $dec * $percent ), 255 );
			$new_hex .= str_pad( dechex( $dec ) , 2, 0, STR_PAD_LEFT );
		}

		return $new_hex;
	}
