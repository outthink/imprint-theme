<?php
/**
* Imprint Initiation Class
*/

class ImprintInit
{
	public function __construct()
	{
		add_action('init', array($this, 'init'));

		// Hook into the 'wp_enqueue_scripts' action
		add_action( 'wp_enqueue_scripts', array($this, 'custom_styles'), 10);
		add_action( 'wp_head', array($this, 'customize_css') );

		// Hook into the 'wp_enqueue_scripts' action for js
		add_action( 'wp_enqueue_scripts', array($this, 'custom_scripts') );
		add_action( 'after_setup_theme', array($this, 'custom_theme_features') );

		//Add Custom Background support
		add_theme_support( 'custom-background');
		add_theme_support( 'customize-selective-refresh-widgets' );

		// do action for Navigation
		add_action('imprint_top_nav', array($this, 'imprint_top_navigation'), 10);

		// do action in footer
		add_action('imprint_do_footer', array($this, 'imprint_footer_markup'), 10);
//		add_action('imprint_do_masthead', array($this, 'imprint_header_breadcrumbs'), 10);
		// TODO: Add in next version
		//add_action('imprint_do_prefooter', array($this, 'imprint_prefooter_markup'), 10);

	}

	function custom_excerpt_length( $length ) {
		if ( $custLength = imprint_option('excerpt_length') ) {
			return $custLength;
		} else {
			return 55;
		}
	}

	function customize_css() {
	?>
		<style type="text/css">
		/* Social Widget Edit Partial Layout*/
		.ot_social_widget .customize-partial-edit-shortcut button{
			top: -4px;
			left: -65px;
		}
		.navbar-social .customize-partial-edit-shortcut button{
			left: -60px;
    		top: -1px;
		}

		section.jumbotron.masthead a .customize-partial-edit-shortcut button{
			left: -55px;
    		top: -5px;
		}
	<?php
		//Layout
		if( imprint_option('website_layout') == 'full' ):
	?>
		.body-wrap{
			max-width: 100%;
		}

	<?php endif;?>
	<?php
		//Logo
		if( imprint_option('site_logo') ):

			$imageID = imprint_option('site_logo');

	        if( $imageID ){

				$imageSrc = $imageID; // For the default value
				if ( is_numeric( $imageID ) ) {
					$imageAttachment = wp_get_attachment_image_src( $imageID, 'full', true );
					$imageSrc = $imageAttachment[0];
				}
	        }

	?>
		.logo-edit .customize-partial-edit-shortcut button{
			left: -36px;
    		top: 9px;
		}
		a.navbar-brand {
			width: <?php echo imprint_option('site_logo_width'); ?>px;
			display: inline-block;
			text-indent: -999em;
			background-image: url(<?php echo esc_url( $imageSrc ); ?>);
			background-repeat: no-repeat;
			background-position: center left;
			background-size: contain;
			margin-right: 10px;
		}
	<?php else:
		if( imprint_option( 'navbar_colors_enabled' ) ):
	?>
		.navbar-inverse a.navbar-brand{
			color: <?php echo imprint_option( 'navbar_link_color' ); ?>;
		}
		.navbar-inverse a.navbar-brand:hover{
			color: <?php echo imprint_option( 'navbar_link_hover_color' ); ?>;
		}
	<?php
		endif;
	endif;

		//Colors
		if( imprint_option( 'enable_color_overlay' ) ):
			$primary_color = imprint_option( 'primary_color' );
			$secondary_color = imprint_option( 'secondary_color' );
			$footer_bck = imprint_option( 'footer_bck_color' );
	?>
		.navbar-inverse{
			background-color: <?php echo $primary_color ?>;
		}
		.navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
		    background-color: <?php echo $secondary_color ?>;
		}
		.btn-primary,
		.button, input.button, input[type=submit]
		{
			background-color: <?php echo $primary_color ?> ;
			border-color: <?php echo $primary_color ?> ;
		}
		.btn-primary:hover,
		.navbar-inverse .navbar-toggle:hover, .navbar-inverse .navbar-toggle:focus,
		.widget.nlsignup_widget .button:hover,
		input.button:hover,
		input[type=submit]:hover,
		.button:focus,
		input.button:focus,
		input[type=submit]:focus,
		.button.focus,
		input.button.focus,
		input[type=submit].focus,
		.button:active,
		input.button:active,
		input[type=submit]:active,
		.button.active,
		input.button.active,
		input[type=submit].active,
		.open > .dropdown-toggle.button,
		.open > .dropdown-toggleinput.button,
		.open > .dropdown-toggleinput[type=submit]{
			background-color: <?php echo $secondary_color ?>;
			border-color: <?php echo $secondary_color ?>;
		}
		#search .close{
			background-color: <?php echo $primary_color ?> ;
			border-color: <?php echo $primary_color ?> ;
		}
		.widget.ot_social_widget i{
			background-color: <?php echo $primary_color ?> ;
			border-color: <?php echo $primary_color ?> ;
		}

		.widget.nlsignup_widget{
			background-color: <?php echo $primary_color ?> !important ;
			border-color: <?php echo $primary_color ?> ;
		}

		.widget.nlsignup_widget .button{
			background-color: <?php echo $primary_color ?> ;

		}
		section.background-primary{
			background-color: <?php echo $primary_color ?>;
		}
		a, .footer a, .pagination > li > a, .pagination > li > span {
			color: <?php echo $primary_color ?>;
		}
		a:hover, a:focus, #blog #posts .post .title a:hover, .pagination > li > a:hover, .pagination > li > span:hover{
			color: <?php echo $secondary_color ?>;
		}
		.imprint_about .imprint-more-link-wrap a.more{
			background-color: <?php echo $primary_color ?> ;
			border-color: <?php echo $primary_color ?> ;
		}
		.imprint_about .imprint-more-link-wrap a.more:hover, .imprint_about .imprint-more-link-wrap a.more:focus{
			background-color: <?php echo $secondary_color ?>;
			border-color: <?php echo $secondary_color ?>;
		}

		.footer{
			background-color: <?php echo  $footer_bck ; ?>;
		}

	<?php endif;
		//Navbar Colors
		if( imprint_option( 'navbar_colors_enabled' ) ):
	?>
		.navbar-inverse{
			background-color: <?php echo imprint_option( 'navbar_background_color' ); ?>;
		}
		.navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
		    background-color: <?php echo imprint_option( 'navbar_active_color' ); ?>;
		}
		.navbar-inverse .navbar-nav > li > a,
		.navbar-inverse .navbar-nav > .active > a,
		.navbar-inverse .navbar-nav > .active > a:hover,
		.navbar-inverse .navbar-nav > .active > a:focus {
		    color: <?php echo imprint_option( 'navbar_link_color' ); ?>;
		}
		.navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus {
		    color: <?php echo imprint_option( 'navbar_link_hover_color' ); ?>;
		}

	<?php
		endif;
	?>
		</style>
	<?php
	}


	function imprint_top_navigation() { ?>
		<?php
			$navbar_color = imprint_option( 'navbar_color' );
			$navbar_logo_width = imprint_option( 'navbar_logo_width' );
			$image = imprint_option( 'navbar_logo' );
			$imageAttachment = wp_get_attachment_image_src( $image, 'full' );
		    $imageSrc = $imageAttachment[0];
			$brand_color = imprint_option( 'brand_color' );
		?>
		<style media="screen">
			.navbar .navbar-brand {
				background-image: url(<?php echo $imageSrc; ?>);
				background-repeat: no-repeat;
				background-position: center left;
				background-size: 100%;
				text-indent: -9999em;
				width: <?php echo $navbar_logo_width; ?>px;
				margin-right: 18px;
			}
			a {
				color: <?php echo $brand_color; ?>;
			}
			a:hover {
				color: <?php echo color_luminance($brand_color, -0.3); ?>;
			}
			.btn-primary, .btn {
				background-color: <?php echo $brand_color; ?>;
				border-color: <?php echo color_luminance($brand_color, -0.2); ?>;
			}
			.btn-primary:hover, .btn:hover {
				background-color: <?php echo color_luminance($brand_color, -0.3); ?>;
				border-color: <?php echo color_luminance($brand_color, -0.4); ?>;
			}
		</style>
		<nav class="navbar <?php echo $navbar_color; ?> header">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar first"></span>
	            <span class="icon-bar middle"></span>
	            <span class="icon-bar last"></span>
	          </button>
	          <span class="logo-edit"></span>
	          <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
	          	<?php bloginfo('name'); ?>
	          </a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
				<?php if (!is_page_template('template-landing.php')): ?>
					<?php wp_nav_menu( array(
						'theme_location' => 'top-navigation',
						'menu_class' 	=> 'nav navbar-nav',
						'container'		=> false,
						'fallback_cb' => false,
						'walker' => new imprint_bootstrap_navwalker()
					)); ?>
					<ul class="nav navbar-nav navbar-right navbar-social">
						<?php
							if( imprint_option( 'social_enabled' ) ){
								imprint_social('li');
							}
						?>

						<?php  if( imprint_option( 'search_enabled' ) ): ?>
						<li class="nav-search">
							<a href="#search">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<?php endif; ?>

					</ul>
				<?php endif; ?>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	<?php }
	function imprint_img_caption_shortcode( $empty, $attr, $content ){
		$attr = shortcode_atts( array(
			'id'      => '',
			'align'   => 'alignnone',
			'width'   => '',
			'caption' => ''
		), $attr );

		if ( 1 > (int) $attr['width'] || empty( $attr['caption'] ) ) {
			return '';
		}

		if ( $attr['id'] ) {
			$attr['id'] = 'id="' . esc_attr( $attr['id'] ) . '" ';
		}

		return '<div ' . $attr['id']
		. 'class="wp-caption ' . esc_attr( $attr['align'] ) . '" '
		. 'style="max-width: ' . ( 10 + (int) $attr['width'] ) . 'px;">'
		. do_shortcode( $content )
		. '<p class="wp-caption-text">' . $attr['caption'] . '</p>'
		. '</div>';

	}
	function imprint_header_breadcrumbs() {
		if (!is_front_page()) { ?>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<?php imprint_breadcrumbs(); ?>
					</div><!-- END 12 -->
				</div><!--END row-->
			</div><!-- END container -->
		<?php
		}
	}
	function imprint_mobile_meta() {
		echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
		$header_scripts = imprint_option('header_scripts');
		if (!empty($header_scripts)) {
			echo $header_scripts;
		}
	}
	function imprint_footer_scripts() { ?>
		<div id="search">
		    <button type="button" class="close">×</button>
		    <form method="get" action="<?php bloginfo('url'); ?>" id="searchform">
		        <input type="search" value="type keyword(s) here" name="s" placeholder="type keyword(s) here" />
		        <button type="submit" class="btn btn-primary">GO!</button>
		    </form>
			<style type="text/css" media="screen">
			#search {
				display:none;
			}
			#search.open input {
				background: none;
			}
			#search.open {
				display:block;
			}
			</style>
		</div>
		<?php
		// scripts get spit out here.
		$footer_scripts = imprint_option('footer_scripts');
		if (!empty($footer_scripts)) {
			echo $footer_scripts;
		}
	}
	// Init Function
	function init() {
		// my additions to the admin bar
		add_action( 'wp_before_admin_bar_render', array($this, 'imprint_additions_to_admin_bar') );
		add_action('wp_head', array($this, 'imprint_mobile_meta'));
		add_action('wp_footer', array($this, 'imprint_footer_scripts'));
		add_action('pre_get_posts', array($this, 'order_books_by_menu_order'));
		add_editor_style();

		// Adding Featured Image Support
		register_nav_menus(array('top-navigation' => __( 'Top Navigation' )));

		// this really should be based on a setting somewhere
		add_image_size( 'blog-image', 780, 380, true ); // adding blog-image for sidebar
		add_image_size( 'about-image', 360, 200, false ); // adding about thumb for sidebar
		add_image_size( 'jumbotron-image', 1400, 800, true ); // adding about thumb for sidebar

		// Adding Shortcode Functionality in Widgets
		add_filter('widget_text', 'do_shortcode');

		// making all videos embed responsively
		add_filter( 'embed_oembed_html', array($this, 'embed_responsively_oembed_filter'), 10, 4 ) ;

		// fixing img_caption so that it doesn't overflow the content area
		add_filter( 'img_caption_shortcode', array($this, 'imprint_img_caption_shortcode'), 10, 3 );
		// excerpt length here
		add_filter( 'excerpt_length', array($this, 'custom_excerpt_length'), 999 );


		if ( ! isset( $content_width ) )
			$content_width = 750;

		register_sidebar(array(
			'name' => 'Blog Sidebar',
			'id' => 'blog',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'before_title' => '<h3 class="widgettitle"><span class="inside">',
			'after_title' => '</span></h3>',
			'after_widget' => '</div>',
		));
		register_sidebar(array(
			'name' => 'Page Sidebar',
			'id' => 'page',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widgettitle"><span class="inside">',
			'after_title' => '</span></h3>',
		));
		if (post_type_exists('book')) {
			register_sidebar(array(
				'name' => 'Book Sidebar',
				'id' => 'book',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widgettitle"><span class="inside">',
				'after_title' => '</span></h3>',
			));
		}
		register_sidebar(array(
			'name' => 'Tabber Widget Area',
			'id' => 'ot_tabber',
			'before_widget' => '<div id="%1$s" class="tab-pane widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widgettitle">',
			'after_title' => '</h3>',
		));
// TODO polish the footer sidebar so that it works well before turning this on
//		register_sidebar(array(
//			'name' => 'Footer Area',
//			'id' => 'footer',
//			'before_widget' => '<div id="%1$s" class="column"><div class="widget widget-inner %2$s">',
//			'after_widget' => '</div></div>',
//			'before_title' => '<h3 class="widgettitle">',
//			'after_title' => '</h3>',
//		));

		do_action('imprint_init');

	} // end init


	function order_books_by_menu_order($query) {
	    // in place of "press" put your custom post type -- you can add additional post types by duplicating the if block.
	    // you can also add more query variables such as 'order'. This is extremely useful
	    // the is_main_query makes sure you don't indiscriminantly filter secondary loops as well.
		if ( !is_admin() && is_post_type_archive('book') && $query->is_main_query() ) {
			 $query->set( 'posts_per_page', '20' );
			 $query->set( 'orderby', 'menu_order' );
			 $query->set( 'order', 'ASC' );
		}
	}

	// the following function will wrap the embedded code in a responsive frame, where the video is 100% width
	function embed_responsively_oembed_filter($html, $url, $attr, $post_ID) {

	    $return = "<style>
				.embed-container {
					position: relative;
					padding-bottom: 56.25%;
					height: 0; overflow: hidden; max-width: 100%;
				}
				.embed-container iframe,
				.embed-container object,
				.embed-container embed {
					position: absolute;
					top: 0;
					left: 0;
					width: 100%;
					height: 100%;
				}
			</style>
			<div class='embed-container'>"
				.$html.
			"</div>";
	    return $return;
	}


	function imprint_additions_to_admin_bar() {
		global $wp_admin_bar;
		//THEN add a sub-link called 'Sublink 1'...
		if ( current_user_can( 'manage_options' ) ) {
    	/* A user with admin privileges */
			$wp_admin_bar->add_menu(array(
				'id'    => 'imprint-link',
				'title' => 'Imprint Options',
				'href'  => admin_url().'admin.php?page=imprint-options',
				'parent'=>'site-name'
			));
			$wp_admin_bar->add_menu(array(
				'id'    => 'pages-link',
				'title' => 'Edit Pages',
				'href'  => admin_url().'edit.php?post_type=page',
				'parent'=>'site-name'
			));

		}

	}


	function custom_theme_features() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		do_action('imprint_theme_features');
	}
	function imprint_prefooter_markup() { ?>
		<?php if (!is_front_page()): ?>
			<?php if (imprint_widgets_count('footer') > 0): ?>
				<section class="grey-wrap prefooter">
					<div class="container">
						<div class="row widgets-count-<?php echo imprint_widgets_count('footer'); ?> widgets-mod-<?php echo imprint_widgets_count('footer')%4; ?>">
							<?php dynamic_sidebar('footer'); ?>
						</div>
					</div>
				</section>
			<?php endif; ?>
		<?php endif; ?>
	<?php }
	function imprint_footer_markup() {
		ob_start(); ?>
		<div class="navbar navbar-static-top footer">
			<div class="navbar-inner">
				<div class="container">
					<div class="pull-left navbar-text legal">
						<?php if (!empty($left_footer = imprint_option('left_footer'))):
							echo $left_footer;
						endif; ?>
					</div>
					<div class="credits navbar-text pull-right">
						<?php if (!empty($right_footer = imprint_option('right_footer'))):
							echo $right_footer;
						endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="search">
		    <button type="button" class="close">×</button>
		    <form method="get" action="<?php bloginfo('url'); ?>" id="searchform">
		        <input type="search" value="type keyword(s) here" name="s" placeholder="Search and press return." />
		        <button type="submit" class="btn btn-primary">GO!</button>
		    </form>
			<style type="text/css" media="screen">
			#search {
				display:none;
			}
			#search.open input {
				background: none;
			}
			#search.open {
				display:block;
			}
			</style>
		</div>
	<?php
	$footerMarkup = ob_get_clean();
	$footerMarkup = apply_filters('imprint_filtered_footer_html', $footerMarkup);
	echo $footerMarkup;
	}
	// Register Scripts
	function custom_scripts() {
		wp_enqueue_script('jquery');

		// -- js/bootstrap.min.js
		wp_register_script( 'imprint_bootstrap_js', get_template_directory_uri().'/js/bootstrap.js', array('jquery'), '1.0', true );
		wp_enqueue_script( 'imprint_bootstrap_js' );

		// -- js/setup.js
		wp_enqueue_script( 'imprint_setup', get_template_directory_uri() . '/js/setup.js', array('jquery'), '1.01', true );
		do_action('imprint_enqueue_script');

	}
	// Register Styles
	function custom_styles() {
		$colorscheme = imprint_option('color_scheme');
		wp_enqueue_style( 'FontAwesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', false, '1.0', 'screen' ); // this is where you will put your stylesheet
		// font family is going to be "google fonts"
		wp_enqueue_style( 'Google Fonts', '//fonts.googleapis.com/css?family=Bitter:400,400italic,700|Open+Sans:400,400italic,600,600italic,700,700italic', false, '1.0', 'all');
		switch ($colorscheme) {
			case '1':
			wp_enqueue_style( 'imprint_bootstrap', get_template_directory_uri().'/css/primary.css', false, '1.56', 'screen' );
				break;
			case '2':
			wp_enqueue_style( 'imprint_bootstrap', get_template_directory_uri().'/css/secondary.css', false, '1.0', 'screen' );
				break;

			case '3':
			wp_enqueue_style( 'imprint_bootstrap', get_template_directory_uri().'/css/tertiary.css', false, '1.0', 'screen' );
				break;
			case '4':
			wp_enqueue_style( 'imprint_bootstrap', get_template_directory_uri().'/css/pink.css', false, '1.0', 'screen' );
				break;

			default:
			wp_enqueue_style( 'Google Fonts', '//fonts.googleapis.com/css?family=Bitter:400,400italic,700|Open+Sans:400,400italic,600,600italic,700,700italic', false, '1.0', 'all');
			wp_enqueue_style( 'imprint_bootstrap', get_template_directory_uri().'/css/primary.css', false, '1.56', 'screen' );
				break;
		}
		wp_enqueue_style( 'imprint_comments', get_template_directory_uri().'/css/comments.css', false, '1.01', 'screen' );
		do_action('imprint_enqueue_style');
	}

}
$ImprintInit = new ImprintInit();
