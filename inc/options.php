<?php
class Imprint_Admin_Settings{
	var $version = '0.9';
	var $updater; // this is not required, but can be helpful
	public function __construct() {
		add_action( 'tf_create_options', array($this, 'imprint_create_options_page') );
		$this->updater = new CGD_EDDSL_Magic("Imprint", "imprint-options", 'http://tools.outthinkgroup.com', $this->version, 'Imprint', 'Out:think Group', false, true );
	}

	function imprint_create_options_page() {

	    // Initialize Titan & options here
		$imprintOptions = TitanFramework::getInstance( 'imprint' );

		$panel = $imprintOptions->createAdminPanel( array(
		    'name' => 'Imprint Options',
			'icon' => 'dashicons-book'
		) );

//		$tab_one->createOption( array(
//			'name' => 'Add Your Newsletter Hook Image',
//			'id' => 'hook_image',
//			'type' => 'upload',
//			'desc' => 'Upload or select the image for your newsletter signup hook.',
//		) );

		$panel->createOption( array(
		    'name' => 'Add your signup hook text here',
		    'id' => 'introtext',
		    'type' => 'editor',
			'rows' => 5,
		    'desc' => 'You can use rich formatting for your description.',
		) );

		$panel->createOption( array(
		    'name' => 'Newsletter Embed Code',
		    'id' => 'embed_code',
		    'type' => 'textarea',
		    'desc' => 'Paste your newsletter embed code here.',
		    'is_code' => true,
		));
		$panel->createOption( array(
		    'name' => 'Left Footer',
		    'id' => 'left_footer',
		    'type' => 'editor',
			'default' => 'All Content &copy; '. date('Y') . ' '. get_bloginfo('name').'</em>',
			'rows' => 2,
		    'desc' => 'Enter text for the left side of the footer.',
		));
		$panel->createOption( array(
		    'name' => 'Right Footer',
		    'id' => 'right_footer',
			'default' => 'Powered by <a href="http://imprinttheme.com">Imprint, a theme for Authors</a>',
		    'type' => 'editor',
			'rows' => 2,
			'desc' => 'Enter text for the right side of the footer.'
		));

/*		$tab_two = $panel->createTab( array(
    		'name' => 'License Information',
		));

		$tab_two->createOption( array(
		    'name' => 'EDD License Option',
		    'id' => 'imprint_license',
		    'type' => 'edd-license',
		    'desc' => 'Please enter your license key to receive updates',
		    'server' => esc_url( 'http://tools.outthinkgroup.com' ),
		    'file' => get_template_directory_uri().'/styles.css', // Assuming this was called inside your main plugin script
		    'item_name' => 'Imprint',
		) );
*/
		$panel->createOption( array(
		    'type' => 'save'
		) );

	}
}
$Imprint_Admin_Settings = new Imprint_Admin_Settings();
