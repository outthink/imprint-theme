<?php
/**
 * Imprint Page meta
 */
class ImprintMetaBoxes
{

    function __construct()
    {
        add_action( 'tf_create_options', array( $this, 'imprint_meta_options' ) );
    }
    function imprint_meta_options() {
        // Create Page Options
        $titanMeta = TitanFramework::getInstance( 'imprint_meta' );

        $pageMetaBox = $titanMeta->createMetaBox( array(
            'name' => 'Additional Page Options',
            'hide_custom_fields' => false
        ) );
        // This function checks the 'post' parameter in the query string to get the post id so we can do some checks.
        if (isset($_GET['post']) && intval($_GET['post']) > 0) {
            // in the case that I need to use the post data, uncomment the following line:
            //$tpost = get_post($pid);
            $pid = intval($_GET['post']);
            $template = get_post_meta($pid, '_wp_page_template', true);
        }
        $pageMetaBox->createOption( array(
            'name' => 'Select a Layout',
            'id' => 'sidebar_layout',
            'type' => 'radio-image',
            'options' => array(
                'global' => get_template_directory_uri() . '/inc/img/global.png',
                'sidebar_left' => get_template_directory_uri() . '/inc/img/sidebar_left.png',
                'sidebar_right' => get_template_directory_uri() . '/inc/img/sidebar_right.png',
                'no_sidebar_center' => get_template_directory_uri() . '/inc/img/no_sidebar_center.png',
                'no_sidebar_full' => get_template_directory_uri() . '/inc/img/no_sidebar.png',
            ),
            'default' => 'global',
        ) );

        $pageMetaBox->createOption( array(
            'name' => 'Sidebar Content',
            'id' => 'sidebar_content',
            'type' => 'editor',
            'desc' => 'Add content here to create a custom widget for this page only.'
        ) );
        $pageMetaBox->createOption( array(
            'name' => 'Hide Sidebar',
            'id' => 'hide_sidebar',
            'type' => 'checkbox',
            'default' => false,
            'desc' => 'Check this box to hide the widgetized sidebar<br><em>(in case you ONLY want to use the sidebar content)</em>',
        ) );

        $pageMetaBox->createOption( array(
            'name' => 'Page Excerpt',
            'id' => 'pageexcerpt',
            'type' => 'editor',
            'desc' => 'This is the Page excerpt that is used in certain parts of your theme.'
        ) );

        // evaluate the "template variable here. If it exists, and the page template is active, this should return true, else it won't show
        //if ( 'template-jumbotron.php' == $template) {
            $pageMetaBox->createOption( array(
                'name' => 'Jumbotron Image',
                'id' => 'jumbotron_image',
                'type' => 'upload',
                'desc' => 'Upload the image for your jumbotron.',
            ) );

            $pageMetaBox->createOption( array(
                'name' => 'Page Title Color',
                'id' => 'headline_color',
                'type' => 'color',
                'desc' => 'Select a color for the page title (in the case of dark images)',
                //'default' => '#555555',
            ) );
//        }

    }
}

new ImprintMetaBoxes();
