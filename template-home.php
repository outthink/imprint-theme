<?php
/*
Template Name: Home Page with Masthead
*/
get_header();
// getting the masthead img id, then getting the image I'll need
$masthead_img = imprint_option('masthead_image', 'imprint-theme-home');
$masthead_img = wp_get_attachment_image_src( $masthead_img, 'jumbotron-image');

$secondary_img = imprint_option('secondary_image', 'imprint-theme-home');
$secondary_img = wp_get_attachment_image_src( $secondary_img, array(500,500));
//var_dump($masthead_img);

$newsletter_thumb = imprint_option('hook_image');
$newsletter_thumb = wp_get_attachment_image_src( $newsletter_thumb, 'medium');

?>
<?php if ( !empty($masthead_img) ): ?>
<style media="screen">
	body section.jumbotron.masthead {
		background-image: url('<?php echo $masthead_img[0]; ?>');
        background-repeat: no-repeat;
        background-size: cover;
	}
    <?php if ( $color = imprint_option('masthead_tagline_color', 'imprint-theme-home') ) {
        echo 'body section.jumbotron.masthead h1 {color: '.$color.'}';
    } ?>
</style>
<?php endif; ?>
<section class="jumbotron masthead">
    <div class="container">
        <div class="row">
            <?php $opt_in = imprint_option('email_opt_in', 'imprint-theme-home'); ?>
                <?php if ($opt_in): ?>
                    <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-10">
                            <h1 class="masthead-tagline">
                                <?php if ($tagline = imprint_option('masthead_tagline', 'imprint-theme-home') ): ?>
                                    <?php echo $tagline; ?>
                                <?php else : ?>
                                    This is where your tagline goes. Make it something memorable and compelling.
                                <?php endif; ?>
                            </h1>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="col-sm-6">
                    <h1>
                        <?php if ($tagline = imprint_option('masthead_tagline', 'imprint-theme-home') ): ?>
                            <?php echo $tagline; ?>
                        <?php else : ?>
                            This is where your tagline goes. Make it something memorable and compelling.
                        <?php endif; ?>
                    </h1>
                <?php endif; ?>
                <?php if ($opt_in): ?>
                    <div class="signup form-inline">
                        <?php echo imprint_option('embed_code'); ?>
                    </div>
                <?php else : ?>
                    <a class="btn btn-lg btn-primary" href="<?php echo imprint_option('tagline_link', 'imprint-theme-home'); ?>">
                        <?php echo imprint_option('tagline_link_text', 'imprint-theme-home'); ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php if (!$opt_in): ?>
	<section class="grey-wrap signup centered ptb">
	    <div class="container">
	        <div class="row col-sm-10 col-sm-offset-1">
	            <div class="ta-center">
	                <?php echo imprint_option('introtext'); ?>
	            </div>
	            <div class="form-inline">
	                <?php echo imprint_option('embed_code'); ?>
	            </div>
	        </div>
	    </div>
	</section>
<?php endif; ?>
<section class="mtb about-wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-1 col-md-6 col-sm-12 pull-right">
                <?php $about_page = imprint_setting('about_page'); ?>
				<h1><?php echo imprint_option('secondary_title', 'imprint-theme-home'); ?></h1>
                <div class="lead">
                    <?php echo wpautop( imprint_option('secondary_text', 'imprint-theme-home') ); ?>
                </div>
			</div>

			<div class="col-lg-5 col-md-6 col-sm-12 pull-left about-image-container">
                <?php if ($secondary_img) { ?>
                    <img src="<?php echo $secondary_img[0]; ?>" alt="<?php $secondary_title; ?>" />
                <?php } else { ?>
                    <img src="<?php bloginfo('template_url'); ?>/images/demo/img-secondary.png">
                <?php } ?>

			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</section>

<?php if (post_type_exists('book') and $featured = imprint_option('featured_book', 'imprint-theme-home')):
$buylinks = imprint_option('use_buy_links', 'imprint-theme-home');
$show_all = imprint_option('show_link_to_all_books', 'imprint-theme-home');
?>
<section class="book-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="inner">
                <?php
                $book = new WP_Query(array(
                    'p' => $featured,
                    'posts_per_page' => 1,
                    'post_type' => 'book'
                ));
                if ($book->have_posts()) : ?>
                <?php while ($book->have_posts()) : $book->the_post(); ?>
                <?php $bookdata = ot_bookdata($post->ID); ?>
                    <div class="row">
                            <div class="col-sm-4">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
                            </div><!-- END col-md-4 -->
                            <div class="col-sm-8">
                                <?php $bookdata = ot_bookdata($featured); ?>
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?>: <?php echo $bookdata['subtitle']; ?></a></h2>
                                <?php the_excerpt(); ?>
                                <?php if ($buylinks): ?>
                                    <?php echo ot_booklinks($bookdata); ?>
                                <?php endif; ?>
                                <p>
                                    <a class="btn btn-primary" href="<?php the_permalink(); ?>">More about the book</a>
                                    <?php if ($show_all): ?>
                                        <a class="btn btn-primary" href="<?php bloginfo('url'); ?>/books">See all books</a>
                                    <?php endif; ?>
                                </p>
                            </div><!-- END col-md-8 -->
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php
// getting the review category here.
    $review_source = imprint_option('review_source', 'imprint-theme-home');
    $reviews = new WP_Query(array(
        'posts_per_page' => '-1',
        'post_type' => 'reviews',
        'tax_query' => array(
    		array(
    			'taxonomy' => 'sources',
    			'field'    => 'id',
    			'terms'    => $review_source,
    		),
    	),
    ));
?>
<?php if ($reviews->have_posts()) : ?>
<section class="background-primary reviews-wrap ptb">
	<div class="container text-center">
		<i class="fa fa-4x fa-quote-left"></i>
		<div class="row">
		<div class="col-sm-10 carousel-wrap col-sm-offset-1 carousel slide" data-interval="12000" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                $c = 1;
                while ($reviews->have_posts()) : $reviews->the_post(); ?>
                    <div class="item<?php if ($c == 1): ?> active<?php endif; ?>">
                        <div class="lead">
                            <?php
                            $review_content = imprint_option('review_content', 'imprint-theme-home');
                            if ('excerpt' == $review_content) {
                                the_excerpt();
                            } else {
                                the_content();
                            } ?>
                            <p class="source">&ndash; <?php the_title(); ?></p>
                        </div>
                    </div>
                <?php $c++; endwhile; ?>
                </div>
		</div>
		</div>
	</div><!-- /.container -->
</section>
<?php endif; ?>
<section id="grey">
    <div class="container">
        <div class="row blog-excerpts">
                <?php
                $blogcount = 3;
            	$numstickies = count(get_option( 'sticky_posts' ));
            	$blogcount = $blogcount - $numstickies;
                $selectposts = new WP_Query(array(
                    'posts_per_page' => $blogcount,
                ));
                if ($selectposts->have_posts()) : while ($selectposts->have_posts()) : $selectposts->the_post(); ?>
                <div class="col-md-4">
                    <?php if (has_post_thumbnail()): ?>
                        <?php the_post_thumbnail('blog-image'); ?>
                    <?php endif; ?>
                    <h3><?php the_title(); ?></h3>
                    <p class="desc">by <?php the_author(); ?> in <b><?php the_category(', ') ?></b></p>
                    <?php the_excerpt(); ?>
                    <p><a class="btn btn-sm btn-primary" href="<?php the_permalink(); ?>">Continue reading...</a></p>
                </div><!--/col-md-4-->
                <?php endwhile; ?>
                <?php endif; ?>
        </div><!--/row -->
    </div>
</section><!--/GREY -->

<?php get_footer(); ?>
