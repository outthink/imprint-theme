<?php
	/*
	Template Name: Jumbotron Page
	*/
	get_header();
?>
<section class="page-content">
<header class="jumbotron jt-page">
	<?php if ($masthead_img = imprint_option('jumbotron_image', 'imprint_meta')):
		if ( $imgsrc = wp_get_attachment_image_src( $masthead_img, 'full') ) { ?>
		<style type="text/css" media="screen">
			.jumbotron {
				position: relative;
			}
			body .jt-page {
				padding-top: 200px;
				padding-bottom: 200px;
			}
			body .jt-page h1 {
				font-size: 36px;
			}
			.jumbotron::after {
				content: "";
				background: url('<?php echo $imgsrc[0]; ?>') top center;
				background-size: cover;
				opacity: 0.2;
				top: 0;
				left: 0;
				bottom: 0;
				right: 0;
				position: absolute;
				z-index: 1;
			}
			<?php if ( $color = imprint_option('headline_color', 'imprint_meta') ) {
				echo '.jt-page h1 {color: '.$color.'}';
			} ?>
			.jumbotron > * {
				position: relative;
				z-index: 2;
			}
		</style>
		<?php } ?>
	<?php endif; ?>
	<div class="bgimg">
		<div class="container">
			<div class="col-md-8 centered">
				<h1 class="pagetitle"><?php the_title(); ?></h1>
				<?php if ($subtext = get_post_meta($post->ID, 'imprint_page_subtext', true)): ?>
					<h2><?php echo $subtext;?></h2>
				<?php endif ?>
			</div><!--end col-md-8 centered -->
		</div><!--end container -->
	</div><!--end bgimg -->
</header><!--end jumbotron -->
	<div class="container page content">
		<div class="row">
			<?php get_template_part('content-page'); ?>
		</div><!--end row -->
	</div><!--end container -->
</section>
<?php get_footer(); ?>
