<?php
global $c;
if ($c > 1): ?>
	<hr>
<?php endif; ?>
<?php $ptype = get_post_type(); ?>
<div <?php post_class('post'); ?>>
	<div>
		<?php if (get_post_type() == 'resources'): ?>
			<?php if ( has_post_thumbnail() ): ?>
				<div class="col-sm-4 mt">
					<?php the_post_thumbnail('medium'); ?>
				</div>
				<?php $content_class = 'col-sm-8'; ?>
			<?php else : ?>
				<?php $content_class = 'col-sm-12'; ?>
			<?php endif; ?>
		<?php endif; ?>
	<div class="<?php echo $content_class; ?>">
		<h2 class="title">
			<?php if (is_single()): ?>
				<?php the_title(); ?>
			<?php else : ?>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<?php endif; ?>
		</h2>
		<?php if (get_post_type() == 'post'): ?>
			<div class="author">
				 <?php
				 $author_email = get_the_author_meta('user_email');
				 $nicename = get_the_author_meta('display_name');
				 // TODO: Add feature to turn this on or off.
				// echo get_avatar( $author_email, 60, '', $nicename, array('class' => 'avatar'));
				 ?>
				<?php echo 'by ' . $nicename;
				$timestamp = get_the_time('U');
				if ($timestamp > strtotime('1 month ago')): ?>, <?php the_date(); ?><?php endif; ?>
				 <?php if ($ptype == 'post'): ?>
				 	in <strong><?php the_category(', ') ?></strong>
				 <?php endif; ?>
			</div>
		<?php endif; ?>
		<?php
		if (is_single() or get_post_type() == 'resources') { ?>
			<div class="content">
				<?php if ( function_exists( 'sharing_display' ) ) {
				    sharing_display( '', true );
				} ?>
				<?php the_content(); 
				echo '<hr>';
				if (get_post_type() == 'post') {
					comments_template();
				} ?>
			</div><!-- / end content -->
		<?php } else { ?>
			<div class="intro row">
				<?php
				// If we have the post thumbnail, spit it out below. If we don't skip this entire section.
				if (has_post_thumbnail()):
					$contentcol = 'col-sm-8';
					// if we're on the single page, it doesn't make sense to link the image, so don't.
					if (! is_single() ): ?>
						<div class="feat-picture col-sm-4">
							<a href="<?php the_permalink(); ?>" class="pic">
								<?php the_post_thumbnail('blog-thumbnail', array('alt' => $post->post_title, 'class' => 'alignleft')); ?>
							</a>
						</div>
					<?php endif; ?>
				<?php else :
					$contentcol = 'col-sm-12';
				endif; ?>
				<div class="<?php echo $contentcol; ?>">
					<?php
					if (imprint_setting('full_content')) {
						the_content('Read More');
					} else {
						if ($pos=strpos($post->post_content, '<!--more-->')) {
							the_content('Read More');
						} else {
							the_excerpt();
						}
					} ?>

				</div>
			</div><!-- / end entro -->
			<?php
		} // end else ?>
		</div>
	</div>
</div>
